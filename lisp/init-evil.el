;;; init-evil.el --- Settings and helpers for package.el -*- lexical-binding: t -*-
;;; Commentary:
;; Emacs State <E>: A state that as closely as possible mimics default
;;   Emacs behaviour, by eliminating all vi bindings. except for C-z,
;;   to re-enter normal state.  -- hjr
;;; Code:

(require-package 'evil)

(evil-mode 1)

(provide 'init-evil)

;;; init-evil.el ends here
