;;; init-yasnippet.el --- Configure yasnippet global behaviour -*- lexical-binding: t -*-
;;; Commentary:
;;; Code:

(require-package 'yasnippet)
(setq yas-snippet-dirs
      '("~/.emacs.d/snippets"                 ;; personal snippets
        ))
(yas-global-mode 1)

(provide 'init-yasnippet)
;;; init-yasnippet.el ends here
